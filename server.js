const express = require('express')
const fetch = require('node-fetch')

function makeServer (log, time, get, conditionsURL) {
  const app = express()
  app.set('json spaces', 4)
  app.get('/api/:latitude,:longitude', (req, res) => {
    time(
      runInParallel({
        wind: time(wind(get, log, conditionsURL, req.params.latitude, req.params.longitude)),
        gust: time(gust(get, log, conditionsURL, req.params.latitude, req.params.longitude))
      })
    ).then((resolvedPromises) => {
      res.json(resolvedPromises)
    })
    // .catch((err) => {
    //   console.error(err)
    //   res.json(err)
    // })
  })
  let server
  return {
    start: () => {
      return new Promise((resolve, reject) => {
        server = app.listen('8080', () => {
          resolve(true)
        })
      })
    },
    stop: () => {
      return new Promise((resolve, reject) => {
        server.close(() => {
          resolve(true)
        })
      })
    }
  }
}

function wind (get, log, baseURL, latitude, longitude) {
  const url = `${baseURL}/${latitude},${longitude}`
  log(url)
  return get(url).then((conditions) => {
    log('Got wind')
    return {msg: 'Got wind'}
  })
}

function gust (get, log, baseURL, latitude, longitude) {
  const url = `${baseURL}/${latitude},${longitude}`
  return get(url).then((conditions) => {
    log('Got gust')
    throw new Error('Gust')
  })
}

function makeCachedFetch (fetch, log, expiresAfter) {
  let cache = {}
  return {
    get: function (url, ...args) {
      if (!cache[url]) {
        log('Fetching...')
        cache[url] = [null, null]
        cache[url][0] = fetch(url, ...args).then((result) => {
          log('Fetched.')
          return result.text().then((text) => {
            cache[url][1] = setTimeout(() => {
              delete cache[url]
              log(`Deleted old cache for ${url}`)
            }, expiresAfter)
            return text
          })
        })
      }
      return cache[url][0]
    },
    clear: function () {
      for (let key in cache) {
        if (cache.hasOwnProperty(key)) {
          clearTimeout(cache[key][1])
          delete cache[key]
        }
      }
    }
  }
}

function makeTimer (logError, timeProperty = 'time', errorProperty = 'error') {
  return {
    time: function (promise) {
      const start = new Date()
      return promise.then((result) => {
        result[timeProperty] = (new Date()) - start
        return result
      }).catch((error) => {
        logError(error)
        let result = {}
        result[timeProperty] = (new Date()) - start
        result[errorProperty] = error.message
        return result
      })
    }
  }
}

function runInParallel (input) {
  let keys = []
  let promises = []
  for (let key in input) {
    keys.push(key)
    promises.push(input[key])
  }
  return Promise.all(promises).then((resolved) => {
    let output = {}
    for (let i = 0; i < resolved.length; i++) {
      output[keys[i]] = resolved[i]
    }
    return output
  })
}

const test = require('tape')

function makeLogger (log) {
  let messages = []
  let currentMessage = 0
  let lastInspectedMessage = 0
  return {
    log: function (...args) {
      currentMessage++
      messages.push(args)
      // log(messages)
    },
    last: function () {
      const slice = messages.slice(lastInspectedMessage, currentMessage)
      // log(["Last", slice, lastInspectedMessage, currentMessage])
      lastInspectedMessage = currentMessage
      return slice
    }
  }
}

let fakeServer
test('before', function (t) {
  const conditionsApp = express()
  conditionsApp.set('json spaces', 4)
  conditionsApp.get('/:latitude,:longitude', (req, res) => {
    res.json({ok: true})
  })
  fakeServer = conditionsApp.listen(8000)
  t.end()
})

test('e2e test success', function (t) {
  const logger = makeLogger(t.comment)
  const timer = makeTimer(logger.log, 'time')
  const cacher = makeCachedFetch(fetch, logger.log, 1500)
  const FAKE_REMOTE = 'http://localhost:8000'
  const server = makeServer(logger.log, timer.time, cacher.get, FAKE_REMOTE)
  server.start().then(() => {
    t.deepEqual(logger.last(), [])
    cacher.get('http://localhost:8080/api/lat,long').then((conditions) => {
      const c = JSON.parse(conditions)
      t.true(c.time);
      ['gust', 'wind'].forEach((key) => {
        t.true(c.hasOwnProperty(key))
        t.true(c[key].hasOwnProperty('time'))
      })
      t.equal(c.wind.msg, 'Got wind')
      t.equal(c.gust.error, 'Gust')
      // t.equal(JSON.parse(conditions), {})
      t.deepEqual(logger.last(), [
        [ 'Fetching...' ],
        [ `${FAKE_REMOTE}/lat,long` ],
        [ 'Fetching...' ],
        [ 'Fetched.' ],
        [ 'Got wind' ],
        [ 'Got gust' ],
        [ new Error('Gust') ],
        [ 'Fetched.' ]
      ])
      server.stop().then(() => {
        // Let any pending cachers run
        setTimeout(() => {
          cacher.clear()
          t.end()
        }, 0)
      })
    })
  })
})

test('e2e test failure', function (t) {
  const logger = makeLogger(t.comment)
  const timer = makeTimer(logger.log, 'time')
  const cacher = makeCachedFetch(fetch, logger.log, 0)
  const FAKE_REMOTE = 'http://localhost:8010' // Nothing serving on this URL
  const server = makeServer(logger.log, timer.time, cacher.get, FAKE_REMOTE)
  server.start().then(() => {
    t.deepEqual(logger.last(), [])
    cacher.get('http://localhost:8080/api/lat,long').then((conditions) => {
      const c = JSON.parse(conditions)
      t.true(c.time);
      ['gust', 'wind'].forEach((key) => {
        t.true(c.hasOwnProperty(key))
        t.true(c[key].hasOwnProperty('error'))
        t.true(c[key].hasOwnProperty('time'))
      })
      server.stop().then(() => {
        // Let any pending cachers run
        setTimeout(() => {
          cacher.clear()
          t.end()
        }, 0)
      })
    })
  })
})

test('after', function (t) {
  fakeServer.close(() => {
    t.end()
  })
})
